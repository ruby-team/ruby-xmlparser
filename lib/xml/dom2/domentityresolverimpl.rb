## -*- Ruby -*-
## XML::DOM::DOMEntityResolverImpl
## 2001 by yoshidam
##

require 'xml/dom2/domentityresolver'
require 'xml/dom2/dominputsource'
require 'open-uri'

module XML
  module DOM
    class DOMEntityResolverImpl
      include DOMEntityResolver

      ## DOMInputSource resolveEntity(publicId, systemId)
      def resolveEntity(publicId, systemId)
        ret = DOMInputSource.new
        ret.publicId = publicId
        ret.systemId = systemId
        ret.byteStream = open(systemId, 'rb')
        ret
      end
    end
  end
end

