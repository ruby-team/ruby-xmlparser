require 'xml/parser'
require 'minitest/autorun'

describe XML::Parser do
  before do
    @parser = XML::Parser.new
  end

  it "can parse an single-element document" do
    @parser.parse("<empty/>")
  end

  it "raises error on a malformed document" do
    raised = false
    begin
      @parser.parse("<invalid")
    rescue XMLParserError
      raised = true
    end
    _(raised).must_equal true
  end
end
